﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Animation : MonoBehaviour
{

    public Animator m_DummyDance;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.T))
        {
            m_DummyDance.enabled = true;
        }

        if (Input.GetKey(KeyCode.Y))
        {
            m_DummyDance.enabled = false;
        }
    }
}
