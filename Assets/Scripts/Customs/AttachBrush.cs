﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class AttachBrush : MonoBehaviour
{
    public GameObject m_Body;
    public GameObject m_SBody;

    // Update is called once per frame

    private void Start()
    {
        combineMeshes(m_SBody);
    }

    /*
    void Update()
    {
        GameObject[] brushs = GameObject.FindGameObjectsWithTag("Brush");

        foreach (GameObject brush in brushs)
        {
            combineMeshes(brush);
            Debug.Log(brush);
        }
    }
    */
    public void combineMeshes(GameObject obj)
    {
        
        Mesh mesh = obj.GetComponent<SkinnedMeshRenderer>().sharedMesh;
        Mesh bodymesh = m_Body.GetComponent<SkinnedMeshRenderer>().sharedMesh;

        Debug.Log("1");
        /*
        mesh.boneWeights = bodymesh.GetAllBoneWeights;
        */


        Vector3 scale = new Vector3(1 / 40, 1 / 40, 1 / 40);
        Matrix4x4 bindPose = GetComponent<SkinnedMeshRenderer>().sharedMesh.bindposes[mesh.boneWeights.Length];
        Matrix4x4 normalizeScale = Matrix4x4.Scale(Vector3.one / 40f);
        bindPose = normalizeScale * bindPose;
        


        Debug.Log("2");

CombineInstance[] totalMesh = new CombineInstance[2];

Debug.Log("3");

totalMesh[0].mesh = bodymesh;
totalMesh[0].transform = m_Body.transform.localToWorldMatrix;
totalMesh[1].mesh = mesh;
totalMesh[1].transform = m_Body.transform.localToWorldMatrix;
mesh.bindposes = bodymesh.bindposes;

Debug.Log("4");

Mesh combinedAllMesh = new Mesh();
Debug.Log(combinedAllMesh);

Debug.Log("5");

combinedAllMesh.CombineMeshes(totalMesh, true);
bodymesh = combinedAllMesh;

Debug.Log("Fin Atteinte");
}
}
