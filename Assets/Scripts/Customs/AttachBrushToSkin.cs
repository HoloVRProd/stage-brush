﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace TiltBrush
{
    public static class Utility
    {
        public static Bounds TransformBounds(this Transform _transform, Bounds _localBounds)
        {
            var center = _transform.TransformPoint(_localBounds.center);

            // transform the local extents' axes
            var extents = _localBounds.extents;
            var axisX = _transform.TransformVector(extents.x, 0, 0);
            var axisY = _transform.TransformVector(0, extents.y, 0);
            var axisZ = _transform.TransformVector(0, 0, extents.z);

            // sum their absolute value to get the world extents
            extents.x = Mathf.Abs(axisX.x) + Mathf.Abs(axisY.x) + Mathf.Abs(axisZ.x);
            extents.y = Mathf.Abs(axisX.y) + Mathf.Abs(axisY.y) + Mathf.Abs(axisZ.y);
            extents.z = Mathf.Abs(axisX.z) + Mathf.Abs(axisY.z) + Mathf.Abs(axisZ.z);

            return new Bounds { center = center, extents = extents };
        }
    }

    public class AttachBrushToSkin : MonoBehaviour
    {
        [SerializeField] private Animator _animator;
        [SerializeField] private string _animatorParameter;

        //skin
        [SerializeField] private SkinnedMeshRenderer _meshSkinSMR;
        [SerializeField] [Range(3, 20)] private int _VertexInterpolationCount = 4;
        [SerializeField] [Range(0, 40)] private int _Smoothing = 10;
        private Mesh _meshSkin;
        private List<Vector3> _skinVertices = new List<Vector3>();
        BoneWeight[] _skinBoneWeights;
        Transform[] _skinBones;

        //brush
        private Mesh _brushMesh;
        private MeshFilter _brushMeshFilter;

        private bool _animationEnabled;
        [SerializeField] private ContainerManager _containerManager;
        [SerializeField] private DropCamWidget _dropCamWidget;

        public bool AnimationEnabled { get => _animationEnabled; }


        private void Awake()
        {
            _meshSkin = Instantiate(_meshSkinSMR).sharedMesh;

            //_meshSkin.GetVertices(_skinVertices);
            //_skinVertices = test_list;
            foreach (var vertex in _meshSkin.vertices)
            {
                //Vector3 temp = vertex;
                //Vector3 temp = _meshSkinSMR.transform.TransformPoint(vertex);

                //temp = transform.InverseTransformPoint(temp);
                _skinVertices.Add(vertex);
                //Debug.DrawLine(Vector3.zero, temp, Color.red, 60f);
            }

            _skinBoneWeights = _meshSkin.boneWeights;
            _skinBones = _meshSkinSMR.bones;


            Transform skinTransform = _meshSkinSMR.transform.parent.gameObject.transform;

            transform.position = skinTransform.position;
            transform.localPosition = skinTransform.localPosition;

            transform.rotation = skinTransform.rotation;
            transform.localRotation = skinTransform.localRotation;

            transform.localScale = skinTransform.localScale;

            transform.eulerAngles = skinTransform.eulerAngles;
            transform.localEulerAngles = skinTransform.localEulerAngles;
            _animationEnabled = false;

        }

        public void Animate(bool b)
        {
            if (!_animator)
            {
                return;
            }

            if (b)
            {
                if (App.ActiveCanvas.BatchManager.CountBatches() != 0)
                {
                    transform.parent.rotation = new Quaternion(0, 0, 0, 0);
                    AttachBrushMeshes();
                }
            }
            

            enabled = b;
            _animator.SetBool(_animatorParameter, b);
            _animationEnabled = b;
            
            StartCoroutine("EnableBrushes", !b);
        }

        private IEnumerator EnableBrushes(bool b)
        {
            if (b)
            {
                yield return new WaitForSeconds(1);
            }
            else {
                yield return new WaitForSeconds(0);
            }

            foreach (var batch in App.ActiveCanvas.BatchManager.AllBatches())
            {
                if (!IsParticle(batch))
                {
                    batch.gameObject.SetActive(b);
                }
            }

            for (int i = 0; i < transform.childCount; i++)
            {
                transform.GetChild(i).gameObject.SetActive(!b);
            }
        }

        /// <summary>
        /// Attacher les mesh du brush au skin
        /// </summary>
        private void AttachBrushMeshes()
        {
            if (_meshSkin == null || App.ActiveCanvas.BatchManager.CountBatches() == 0)
                return;
#if PROFILE
            System.DateTime timeB= System.DateTime.Now;
#endif
            //destroy childs
            for (int i = 0; i < transform.childCount; i++)
            {
                Destroy(transform.GetChild(i).gameObject);
            }

            foreach (var batch in App.ActiveCanvas.BatchManager.AllBatches())
            {
                //créer un objet enfant pour chaque brush
                GameObject batchClone = new GameObject("AnimatedClonedBrush" + batch.gameObject.name);
                batchClone.transform.parent = transform;

                /**+/
                AttachBrushMesh(batch, ref batchClone);
                /*/
                //NR_AttachBrushMesh(batch, ref batchClone);
                AttachBrushMeshWithContainer(batch,  ref batchClone);
                /**/
            }

#if PROFILE
            System.DateTime timeE = System.DateTime.Now;
            System.TimeSpan ts = timeE - timeB;
            Debug.Log("ELAPSED : " + ts.TotalMilliseconds);
#endif
        }

        void SmoothBoneWeights(List<BoneWeight> InBoneWeights,ref List<BoneWeight> OutBoneWeights, Vector3[] Vertices, int neighbours)
        {
            //BoneWeight[] result = new BoneWeight[boneWeights.Count()];
            for (int H = 0; H < Vertices.Count(); H++)
            {
                //int Vcount = _VertexInterpolationCount;
                float maxDist = Mathf.Infinity;

                List<Vector3> nearestVertices = new List<Vector3>();
                List<float> distance = new List<float>();
                List<int> I_all = new List<int>();
                List<float> W_all = new List<float>();

                //Populer les arrays avec la position, distance, les 4 indices et 4 poids des N vertices du skin les plus proches
                // a la vertice de la brush
                //Debug.DrawLine(Vector3.zero, vertexBrushPositionInContainer, Color.green, 60f);
                for (int I = 0; I < Vertices.Count(); I++)
                {
                    if(Vertices[I] != Vertices[H])
                    {
                        //Debug.DrawLine(Vector3.zero, vertex, Color.red, 60f);
                        Vector3 vertex = Vertices[I];
                        float Curren_Dist = (Vertices[H] - vertex).sqrMagnitude;
                        if (Curren_Dist < maxDist)
                        {
                            if (nearestVertices.Count() < neighbours)
                            {
                                nearestVertices.Add(vertex);
                                distance.Add(Curren_Dist);
                                InBoneWeights[I].AddToIntermediateLists(ref I_all, ref W_all);
                            }
                            else
                            {
                                int index = 0;
                                float dist = 0;
                                for (int Y = 0; Y < neighbours; Y++)
                                {
                                    float N2 = (Vertices[H] - nearestVertices[Y]).sqrMagnitude;
                                    if (N2 > dist)
                                    {
                                        dist = N2;
                                        index = Y;
                                    }
                                }
                                nearestVertices[index] = vertex;
                                distance[index] = Curren_Dist;
                                maxDist = dist;
                                InBoneWeights[I].EditIntermedateLists(ref I_all, ref W_all, index);
                            }
                        }
                    }
                }

                /*foreach(Vector3 V in nearestVertices)
                {
                    Debug.DrawLine(Vertices[H], V, Color.green, 20f);
                }*/



                //Faire une interpolation de tous les poids par indexe et la distance et ajouter à _Index et _ Wieght
                //Le vertex le plus proche donne plus de ses poids
                List<int> _Index = new List<int>();
                List<float> _Wieght = new List<float>();
                bool _isAllUsed = false;
                while (!_isAllUsed)
                {
                    int divider = 0;
                    float W_temp = 0;
                    int I_temp = -1;
                    for (int I = 0; I < neighbours * 4; I++)
                    {
                        if (I_temp == -1 && W_all[I] >= 0.0001f)
                        {
                            W_temp = W_all[I];
                            I_temp = I_all[I];
                            W_all[I] = 0f;
                            I_all[I] = 0;
                            divider++;
                        }
                        else
                        {
                            if (I_all[I] == I_temp && W_all[I] >= 0.0001f)
                            {
                                W_temp += W_all[I];
                                W_all[I] = 0f;
                                I_all[I] = 0;
                                divider++;
                            }
                        }
                    }
                    if (W_temp >= 0.0001f && !float.IsNaN(W_temp) && I_temp != -1)
                    {
                        _Index.Add(I_temp);
                        _Wieght.Add(W_temp / divider);
                    }
                    foreach (float W in W_all)
                    {
                        if (W < 0.0001f)
                        {
                            _isAllUsed = true;
                        }
                        else
                        {
                            _isAllUsed = false;
                            break;
                        }
                    }
                }

                BoneWeight_Extension.ParallelSortByWeight(ref _Index, ref _Wieght);

                OutBoneWeights.Add(BoneWeight_Extension.AddBoneWieghtNormalise(_Index, _Wieght));
            }
        }
            
        private void AttachBrushMeshWithContainer(Batch batch, ref GameObject go)
        {
            _brushMeshFilter = batch.gameObject.GetComponent<MeshFilter>();
           
            if (!_brushMeshFilter || IsParticle(batch))
            {
                return;
            }

            //compute:
            //          + index of container intersection
            //          + list of intersection subset
            //          + list of mesh not in container (environnement meshes)
            List<int> listIndexContainer = new List<int>();
            List<BatchSubset> listIntersection = new List<BatchSubset>();
            List<Mesh> listEnvMesh = new List<Mesh>();
            foreach (var group in batch.m_Groups)
            {
                Bounds brushSubsetBound = Utility.TransformBounds(batch.transform, group.m_Bounds);

                _containerManager.CheckIntersection(brushSubsetBound);
                if (_containerManager.ExistIntersection())
                {
                    listIndexContainer.Add(_containerManager.IndexIntersection);
                    listIntersection.Add(group);
                }
                else
                {
                    //maillage de l'environnement
                    Mesh mesh = new Mesh();
                    batch.Geometry.CopyToMesh(mesh, group.m_StartVertIndex, group.m_VertLength, group.m_iTriIndex, group.m_nTriIndex);
                    listEnvMesh.Add(mesh);
                }
            }

            //get all the vertices of the batch 
            Vector3[] batchVertices;
            if (batch.Geometry.m_Vertices != null)
            {
                batchVertices = batch.Geometry.m_Vertices.ToArray();
            }
            else
            {
                batchVertices = batch.Geometry.GetBackingMesh().vertices;
            }

            //compute vertices weights for each group
            List<BoneWeight> SkinlistBoneWeight = new List<BoneWeight>();
            List<BoneWeight> SmoothlistBoneWeight = new List<BoneWeight>();
            List<int> listBoneWeightIndexes = new List<int>();
            int iContainer = 0;

            foreach (var group in batch.m_Groups)
            {
                for (int i = group.m_StartVertIndex; i < group.m_StartVertIndex + group.m_VertLength; i++)
                {
                    List<BoneWeight> BW_temp = new List<BoneWeight>();
                    foreach ( var container in _containerManager.Containers)
                    {
                        Vector3 temp = batchVertices[i];
                        temp = batch.transform.TransformPoint(temp);
                        //Debug.DrawLine(Vector3.one, temp, Color.white, 60f);
                        if (container.gameObject.GetComponent<MeshRenderer>().bounds.Contains(temp))
                        {
                            ///Container.DrawBounds(container.gameObject.GetComponent<MeshRenderer>().bounds, 30); ///Debug
                            UpdateBoneWeightsWithContainer(ref BW_temp, temp, container);
                        }
                    }
                    if (BW_temp.Any())
                    {
                        SkinlistBoneWeight.Add(BW_temp.AverageBoneWeightList());
                    }
                    else
                    {
                        SkinlistBoneWeight.Add(new BoneWeight());
                    }
                }
            }

            //Smoothing
            if(_Smoothing > 0)
            {
                SmoothBoneWeights(SkinlistBoneWeight,ref SmoothlistBoneWeight, batchVertices, _Smoothing);
            }
            else
            {
                SmoothlistBoneWeight = SkinlistBoneWeight;
            }

            //create bindposes
            Matrix4x4[] bindPoses = new Matrix4x4[_meshSkinSMR.bones.Length];
            for (int i = 0; i < bindPoses.Length; i++)
            {
                bindPoses[i] = _skinBones[i].worldToLocalMatrix * _brushMeshFilter.transform.localToWorldMatrix;
            }

            //create skinned mesh renderer
            SkinnedMeshRenderer meshSMR = go.AddComponent<SkinnedMeshRenderer>();
            //Mesh meshClone = _brushMeshFilter.sharedMesh;
            Mesh meshClone = new Mesh();
            batch.Geometry.CopyToMesh(meshClone);
            meshClone.RecalculateBounds();
            meshClone.bindposes = bindPoses;
            meshClone.boneWeights = SmoothlistBoneWeight.ToArray();

            meshSMR.sharedMesh = meshClone;
            meshSMR.bones = _meshSkinSMR.bones;
            meshSMR.rootBone = _meshSkinSMR.rootBone;
            meshSMR.material = batch.InstantiatedMaterial;

            CreateEnvironmentObject(ref listEnvMesh, batch, go.transform);
        }

        void UpdateBoneWeightsWithContainer(ref List<BoneWeight> listBoneWeight, Vector3 vertexBrushPositionInContainer, Container Container)
        {
            int Vcount = _VertexInterpolationCount;
            float maxDist = Mathf.Infinity;

            List<Vector3> nearestVertices = new List<Vector3>();
            List<float> distance = new List<float>();
            List<int> I_all = new List<int>();
            List<float> W_all = new List<float>();

            //Populer les arrays avec la position, distance, les 4 indices et 4 poids des N vertices du skin les plus proches
            // a la vertice de la brush
            //Debug.DrawLine(Vector3.zero, vertexBrushPositionInContainer, Color.cyan, 60f);
            foreach (var A in Container.Skin_indexes)
            {
                Vector3 vertex = _skinVertices[A];
                vertex = _meshSkinSMR.transform.TransformPoint(vertex);
                //Debug.DrawLine(Vector3.zero, vertex, Color.yellow, 60f);
                float Curren_Dist = (vertexBrushPositionInContainer - vertex).sqrMagnitude;
                if (Curren_Dist < maxDist)
                {
                    if(nearestVertices.Count() < Vcount)
                    {
                        nearestVertices.Add(vertex);
                        distance.Add(Curren_Dist);
                        _skinBoneWeights[A].AddToIntermediateLists(ref I_all, ref W_all);
                    }
                    else
                    {
                        int index = 0;
                        float dist = 0;
                        for (int I = 0; I < Vcount; I++)
                        {
                            float N2 = (vertexBrushPositionInContainer - nearestVertices[I]).sqrMagnitude;
                            if (N2 > dist)
                            {
                                dist = N2;
                                index = I;
                            }
                        }
                        nearestVertices[index] = vertex;
                        distance[index] = Curren_Dist;
                        maxDist = dist;
                        _skinBoneWeights[A].EditIntermedateLists(ref I_all, ref W_all, index);
                    }
                }
            }

            /*foreach (Vector3 V in nearestVertices)
            {
                Debug.DrawLine(vertexBrushPositionInContainer, V, Color.red, 30f);
            }*/

            //Faire une interpolation de tous les poids par indexe et la distance et ajouter à _Index et _ Wieght
            //Le vertex le plus proche donne plus de ses poids
            List<int> _Index = new List<int>();
            List<float> _Wieght = new List<float>();
            float total_dist = distance.Sum();
            bool _isAllUsed = false;
            while (!_isAllUsed)
            {
                int divider = 0;
                float W_temp = 0;
                int I_temp = -1;
                for (int I = 0; I < Vcount * 4; I++)
                {
                    if (I_temp == -1 && W_all[I] >= 0.0001f)
                    {
                        W_temp = W_all[I] * ((total_dist - distance[I % Vcount]) / total_dist);
                        I_temp = I_all[I];
                        W_all[I] = 0f;
                        I_all[I] = 0;
                        divider++;
                    }
                    else
                    {
                        if (I_all[I] == I_temp && W_all[I] >= 0.0001f)
                        {
                            W_temp += W_all[I] * ((total_dist - distance[I % Vcount]) / total_dist);
                            W_all[I] = 0f;
                            I_all[I] = 0;
                            divider++;
                        }
                    }
                }
                if (W_temp >= 0.0001f && !float.IsNaN(W_temp) && I_temp != -1)
                {
                    _Index.Add(I_temp);
                    _Wieght.Add(W_temp / divider);
                }
                foreach (float W in W_all)
                {
                    if (W < 0.0001f)
                    {
                        _isAllUsed = true;
                    }
                    else
                    {
                        _isAllUsed = false;
                        break;
                    }
                }
            }

            BoneWeight_Extension.ParallelSortByWeight(ref _Index, ref _Wieght);

            listBoneWeight.Add(BoneWeight_Extension.AddBoneWieghtNormalise(_Index, _Wieght));
        }

        private void AttachBrushMesh(in Batch batch, ref GameObject go)
        {
            _brushMeshFilter = batch.gameObject.GetComponent<MeshFilter>();

            if (!_brushMeshFilter)
            {
                return;
            }

            _brushMesh = _brushMeshFilter.sharedMesh;

           
            BoneWeight[] boneWeights = new BoneWeight[_brushMesh.vertexCount];
            Matrix4x4[] bindPoses = new Matrix4x4[_meshSkinSMR.bones.Length];

            for (int i = 0; i < _brushMesh.vertices.Length; i++)
            {
                //Debug.Log("vertex : " + i);
                //UpdateBoneWeightsOrig(skinVertices, boneWeights, skinBoneWeights, i, transform.TransformPoint(_propMesh.vertices[i]));
                /**/
                Vector3 temp = _brushMesh.vertices[i];
                temp = batch.transform.TransformPoint(temp);
                temp = transform.InverseTransformPoint(temp);

                UpdateBoneWeights(ref boneWeights, i, temp);
            }

            for (int i = 0; i < bindPoses.Length; i++)
            {
                bindPoses[i] = _skinBones[i].worldToLocalMatrix * _brushMeshFilter.transform.localToWorldMatrix;
            }

            SkinnedMeshRenderer meshSMR = go.AddComponent<SkinnedMeshRenderer>();
            Mesh meshClone = new Mesh();
            batch.Geometry.CopyToMesh(meshClone);
            meshClone.RecalculateBounds();

            meshClone.bindposes = bindPoses;
            meshClone.boneWeights = boneWeights;

            meshSMR.sharedMesh = meshClone;
            meshSMR.bones = _meshSkinSMR.bones;
            meshSMR.rootBone = _meshSkinSMR.rootBone;
            meshSMR.material = batch.InstantiatedMaterial;
        }

        void UpdateBoneWeights(ref BoneWeight[] boneWeights, int meshIndex, Vector3 vertexBrushPositionInContainer)
        {
            float minDistance = Mathf.Infinity;
            float minDistance2 = Mathf.Infinity;
            Vector3 nearestVertex = Vector3.zero;
            Vector3 nearestVertex2 = Vector3.zero;
            Vector3 direction = Vector3.zero;

            int i = 0;
            int inearest = 0, inearest2 = 0;
            //find the two closest vertices
            foreach (var vertex in _skinVertices)
            {
                direction = vertexBrushPositionInContainer - vertex;
                float dist = direction.sqrMagnitude;
                if (dist < minDistance)
                {
                    nearestVertex = vertex;
                    minDistance = dist;
                    inearest = i;
                }
                else if (dist < minDistance2)
                {
                    nearestVertex2 = vertex;
                    minDistance2 = dist;
                    inearest2 = i;
                }
                i++;
            }

            if (nearestVertex != Vector3.zero)
            {
                //Debug.Log("MinDistance " + minDistance);
                Vector3 bv = vertexBrushPositionInContainer - nearestVertex2;
                Vector3 av = vertexBrushPositionInContainer - nearestVertex;
                float t = av.sqrMagnitude / (av.sqrMagnitude + bv.sqrMagnitude);
                //Debug.Log("t " + t) ;
                boneWeights[meshIndex] = _skinBoneWeights[inearest];

                //Debug.Log("a " + skinBoneWeights[nearestVertex].weight0);
                //Debug.Log("b " + skinBoneWeights[nearestVertex2].weight0);
                boneWeights[meshIndex].weight0 = Mathf.Lerp(_skinBoneWeights[inearest].weight0, _skinBoneWeights[inearest2].weight0, t);
                boneWeights[meshIndex].weight1 = Mathf.Lerp(_skinBoneWeights[inearest].weight1, _skinBoneWeights[inearest2].weight1, t);
                boneWeights[meshIndex].weight2 = Mathf.Lerp(_skinBoneWeights[inearest].weight2, _skinBoneWeights[inearest2].weight2, t);
                boneWeights[meshIndex].weight3 = Mathf.Lerp(_skinBoneWeights[inearest].weight3, _skinBoneWeights[inearest2].weight3, t);
                //Debug.Log("w " + boneWeights[meshIndex].weight0);
            }
            else
            {
                boneWeights[meshIndex].weight0 = 0.5f;
                Debug.LogError("No nearest vertex");
            }
        }

        void CreateEnvironmentObject(ref List<Mesh> listEnvMesh, in Batch batch, in Transform transform)
        {
            if (listEnvMesh.Count == 0)
            {
                return;
            }
            GameObject goEnv = new GameObject("Environment");
            goEnv.transform.SetParent(batch.transform.parent, false);
            goEnv.transform.SetParent(transform, true);

            //create a mesh from meshes not inside container
            MeshFilter meshFilter = goEnv.AddComponent<MeshFilter>();
            CombineInstance[] combine = new CombineInstance[listEnvMesh.Count];
            List<Vector3> listVert = new List<Vector3>();
            for (int i = 0; i < listEnvMesh.Count; i++)
            {
                combine[i].mesh = listEnvMesh[i];
                combine[i].transform = goEnv.transform.localToWorldMatrix;
                foreach (var vertex in listEnvMesh[i].vertices)
                {
                    listVert.Add(new Vector3(vertex.x, vertex.y, vertex.z));
                }
            }

            Mesh meshEnv = new Mesh();
            meshEnv.CombineMeshes(combine);
            meshEnv.vertices = listVert.ToArray();
            meshFilter.mesh = meshEnv;

            MeshRenderer envMR = goEnv.AddComponent<MeshRenderer>();
            envMR.material = batch.InstantiatedMaterial;
        }

        public void Animate()
        {
            Animate(!_animationEnabled);
        }

        private bool IsParticle(Batch batch)
        {
            BrushDescriptor desc = BrushCatalog.m_Instance.GetBrush(batch.ParentPool.m_BrushGuid);
            return desc.m_BrushPrefab.GetComponent<GeniusParticlesBrush>() != null;
        }
        /*
        /// <summary>
        /// Update the boneweight of a vertex, by choosing the closest. Needs a lot of arrays to avoid getting them inside the loop and causing a lot of GC.
        /// </summary>
        /// <param name="skinVertex">baked skinMesh vertex</param>
        /// <param name="boneWeights">an array of preallocated boneWeights</param>
        /// <param name="skinBoneWeights">baked skinMesh bone weights</param>
        /// <param name="meshIndex">the current vertex index on the mesh</param>
        /// <param name="worldPosition">the position of this vertex, in world coordinates</param>
        void UpdateBoneWeightsOrig(Vector3[] skinVertex, BoneWeight[] boneWeights, BoneWeight[] skinBoneWeights, int meshIndex, Vector3 worldPosition)
        {
            float minDistance = Mathf.Infinity;
            int nearestVertex = -1;
            Vector3 direction = Vector3.zero;

            //find the closest vertex
            for (int i = 0; i < skinVertex.Length; i++)
            {
                direction = worldPosition - skinVertex[i];
                float dist = direction.sqrMagnitude;
                if (dist < minDistance)
                {
                    nearestVertex = i;
                    minDistance = dist;
                }
            }

            if (nearestVertex != -1)
            {
                boneWeights[meshIndex] = skinBoneWeights[nearestVertex];
            }
        }
        */
    }
}