﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace UnityEngine
{
    static class BoneWeight_Extension
    {
        public static BoneWeight AverageBoneWeightList(this List<BoneWeight> BoneList)
        {
            //__TODO__ List by indexes and average the weights
            //int Vcount = I_all.Count();

            List<float> Weights = new List<float>();
            List<int> Indexes = new List<int>();

            foreach(BoneWeight B in BoneList)
            {
                B.AddToIntermediateLists(ref Indexes, ref Weights);
            }

            Interpolation(ref Indexes, ref Weights);

            ParallelSortByWeight(ref Indexes, ref Weights);
        /*
            Result.boneIndex0 = Indexes[0];
            Result.boneIndex1 = Indexes[1];
            Result.boneIndex2 = Indexes[2];
            Result.boneIndex3 = Indexes[3];

            Result.weight0 = Weights[0];
            Result.weight1 = Weights[1];
            Result.weight2 = Weights[2];
            Result.weight3 = Weights[3];
        */
            return AddBoneWieghtNormalise(Indexes,Weights);
        }

        private static void Interpolation(ref List<int> I_all, ref List<float> W_all)
        {
            //Faire une interpolation de tous les poids par indexe et ajouter à _Index et _ Wieght
            List<int> _Index = new List<int>();
            List<float> _Weight = new List<float>();
            bool _isAllUsed = false;
            while (!_isAllUsed)
            {
                int divider = 0;
                float W_temp = 0;
                int I_temp = -1;
                for (int I = 0; I < I_all.Count(); I++)
                {
                    if (I_temp == -1)
                    {
                        if (W_all[I] >= 0.0001f)
                        {
                            W_temp = W_all[I];
                            I_temp = I_all[I];
                            W_all[I] = 0f;
                            I_all[I] = 0;
                            divider++;
                        }
                    }
                    else
                    {
                        if (I_all[I] == I_temp && W_all[I] >= 0.0001f)
                        {
                            W_temp += W_all[I];
                            W_all[I] = 0f;
                            I_all[I] = 0;
                            divider++;
                        }

                    }
                }
                if (W_temp >= 0.0001f && !float.IsNaN(W_temp) && I_temp != -1)
                {
                    _Index.Add(I_temp);
                    _Weight.Add(W_temp / divider);
                }
                foreach (float W in W_all)
                {
                    if (W < 0.0001f)
                    {
                        _isAllUsed = true;
                    }
                    else
                    {
                        _isAllUsed = false;
                        break;
                    }
                }
            }
            W_all = _Weight;
            I_all = _Index;
        }

        private static void InterpolationByDistance(ref List<int> I_all, ref List<float> W_all, List<float> distance)
        {
            List<int> _Index = new List<int>();
            List<float> _Weight = new List<float>();
            float total_dist = distance.Sum();
            int Vcount = _Index.Count();
            bool _isAllUsed = false;
            while (!_isAllUsed)
            {
                int divider = 0;
                float W_temp = 0;
                int I_temp = -1;
                for (int I = 0; I < Vcount; I++)
                {
                    if (I_temp == -1 && W_all[I] >= 0.0001f)
                    {
                        W_temp = W_all[I] * ((total_dist - distance[I % Vcount /4]) / total_dist);
                        I_temp = I_all[I];
                        W_all[I] = 0f;
                        I_all[I] = 0;
                        divider++;
                    }
                    else
                    {
                        if (I_all[I] == I_temp && W_all[I] >= 0.0001f)
                        {
                            W_temp += W_all[I] * ((total_dist - distance[I % Vcount /4]) / total_dist);
                            W_all[I] = 0f;
                            I_all[I] = 0;
                            divider++;
                        }
                    }
                }
                if (W_temp >= 0.0001f && !float.IsNaN(W_temp) && I_temp != -1)
                {
                    _Index.Add(I_temp);
                    _Weight.Add(W_temp / divider);
                }
                foreach (float W in W_all)
                {
                    if (W < 0.0001f)
                    {
                        _isAllUsed = true;
                    }
                    else
                    {
                        _isAllUsed = false;
                        break;
                    }
                }
            }
            W_all = _Weight;
            I_all = _Index;
        }

        public static void ParallelSortByWeight(ref List<int> _Index, ref List<float> _Weight) //Could be improved but it is good enough so far
        {
            bool sorted = false;
            int size = _Weight.Count();
            while (!sorted)
            {
                sorted = true;
                for (int I = 0; I < size - 1; I++)
                {
                    if (_Weight[I] < _Weight[I + 1])
                    {
                        float temp = _Weight[I];
                        _Weight[I] = _Weight[I + 1];
                        _Weight[I + 1] = temp;
                        int tempint = _Index[I];
                        _Index[I] = _Index[I + 1];
                        _Index[I + 1] = tempint;
                        sorted = false;
                    }
                }
            }
        }
        public static BoneWeight AddBoneWieghtNormalise(List<int> _Index, List<float> _Weight)
        {
            float multiplier = 0;
            int Size = _Index.Count();
            BoneWeight BoneW = new BoneWeight();
            if (Size >= 1)
            {
                BoneW.boneIndex0 = _Index[0];
                BoneW.weight0 = _Weight[0];
                multiplier += _Weight[0];
            }
            else
            {
                BoneW.boneIndex0 = 0;
                BoneW.weight0 = 1;
            }
            if (Size >= 2)
            {
                BoneW.boneIndex1 = _Index[1];
                BoneW.weight1 = _Weight[1];
                multiplier += _Weight[1];
            }
            else
            {
                BoneW.boneIndex1 = 0;
                BoneW.weight1 = 0;
            }
            if (Size >= 3)
            {
                BoneW.boneIndex2 = _Index[2];
                BoneW.weight2 = _Weight[2];
                multiplier += _Weight[2];
            }
            else
            {
                BoneW.boneIndex2 = 0;
                BoneW.weight2 = 0;
            }
            if (Size >= 4)
            {
                BoneW.boneIndex3 = _Index[3];
                BoneW.weight3 = _Weight[3];
                multiplier += _Weight[3];
            }
            else
            {
                BoneW.boneIndex3 = 0;
                BoneW.weight3 = 0;
            }

            //Normaliser la somme des poids à 1.
            BoneW.weight0 = BoneW.weight0 / multiplier;
            BoneW.weight1 = BoneW.weight1 / multiplier;
            BoneW.weight2 = BoneW.weight2 / multiplier;
            BoneW.weight3 = BoneW.weight3 / multiplier;

            return BoneW;
        }

        public static void AddToIntermediateLists(this BoneWeight bone,ref List<int> intermediate_Indexes,ref List<float> intermediate_Weights)
        {
            intermediate_Indexes.Add(bone.boneIndex0);
            intermediate_Indexes.Add(bone.boneIndex1);
            intermediate_Indexes.Add(bone.boneIndex2);
            intermediate_Indexes.Add(bone.boneIndex3);

            intermediate_Weights.Add(bone.weight0);
            intermediate_Weights.Add(bone.weight1);
            intermediate_Weights.Add(bone.weight2);
            intermediate_Weights.Add(bone.weight3);
        }

        public static void EditIntermedateLists(this BoneWeight bone,ref List<int> intermediate_Indexes,ref List<float> intermediate_Weights, int Index)
        {
            intermediate_Indexes[Index*4] = bone.boneIndex0;
            intermediate_Indexes[Index*4+1] = bone.boneIndex1;
            intermediate_Indexes[Index*4+2] = bone.boneIndex2;
            intermediate_Indexes[Index*4+3] = bone.boneIndex3;

            intermediate_Weights[Index*4] = bone.weight0;
            intermediate_Weights[Index*4+1] = bone.weight1;
            intermediate_Weights[Index*4+2] = bone.weight2;
            intermediate_Weights[Index*4+3] = bone.weight3;
        }
    }
}