﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathCreation;
using TiltBrush;

public class PathFollower : MonoBehaviour
{
    public PathCreator pathCreator;
    public EndOfPathInstruction endOfPathInstruction;
    public float _speed = 5;
    float distanceTravelled;

    private Camera _camera;
    private void Awake()
    {
        _camera = GetComponent<Camera>();
    }

    void Update()
    {
        if (pathCreator != null)
        {
            distanceTravelled += _speed * Time.deltaTime;
            transform.position = pathCreator.path.GetPointAtDistance(distanceTravelled, endOfPathInstruction);
        }
    }
}
