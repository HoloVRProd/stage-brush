﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TiltBrush;

public class PathScale : MonoBehaviour
{
    [SerializeField] private DropCamWidget _dropCamWidget;
    private Transform _path;
    
    private void Awake()
    {
        _path = transform.GetChild(0);
    }

    public void ChangeScale()
    {
        Vector3 target = _dropCamWidget.GetTarget();
        if (target != Vector3.zero)
        {
            float rInit = 6; //radius of infinity path
            float scaleInit = 1;

            float r = Vector3.Distance(transform.position, target);
            float scale = (r * scaleInit) / rInit;
            Vector3 vScale = new Vector3(scale, scale, scale);
            _path.localScale = vScale;
        }
    }
}
