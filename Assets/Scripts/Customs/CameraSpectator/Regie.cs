﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TiltBrush;

public class Regie : MonoBehaviour
{
    [SerializeField] private GameObject _camera;
    [SerializeField] private GameObject _brushAnimator;
    [SerializeField] private float _lateralSpeed;
    [SerializeField] private float _forwardSpeed;
    public AnimationCurve _animationCurve;
    private float _curveDeltaTime = 0.0f;

    // Update is called once per frame
    void Update()
    {
        if (!_camera.activeSelf)
        {
            return;
        }

        if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.RightArrow) ||
            Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.DownArrow))
        {
            _curveDeltaTime = 0;
        }

        
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            _curveDeltaTime += Time.deltaTime;
            float speed = _animationCurve.Evaluate(_curveDeltaTime) * _lateralSpeed;
            _camera.transform.Translate(-Vector3.right * Time.deltaTime * speed, _camera.transform);
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            _curveDeltaTime += Time.deltaTime;
            float speed = _animationCurve.Evaluate(_curveDeltaTime) * _lateralSpeed;
            _camera.transform.Translate(Vector3.right * Time.deltaTime * speed, _camera.transform);
        }

        if (Input.GetKey(KeyCode.UpArrow))
        {
            _camera.transform.Translate(Vector3.forward * Time.deltaTime * _forwardSpeed, _camera.transform);
        }

        if (Input.GetKey(KeyCode.DownArrow))
        {
            _camera.transform.Translate(-Vector3.forward * Time.deltaTime * _forwardSpeed, _camera.transform);
        }
    }

    public bool AnimationEnabled()
    {
        return _brushAnimator.GetComponent<AttachBrushToSkin>().AnimationEnabled;
    }
}
