﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TiltBrush;
public class Container : MonoBehaviour
{
    public List<int> Skin_indexes { get; set; } = new List<int>();
    public List<int> Brush_indexes { get; set; } = new List<int>();

    private void Awake()
    {
        MeshRenderer cubeMesh = gameObject.GetComponent<MeshRenderer>();


        int i = 0;
        GameObject skin = GetSkin();

        Vector3[] skinMeshVertices = skin.GetComponent<SkinnedMeshRenderer>().sharedMesh.vertices;
        foreach (var vertex in skinMeshVertices)
        {
            //DrawBounds(cubeMesh.bounds, 60); /// debug
            //Debug.DrawLine(skin.transform.position, skin.transform.TransformPoint(vertex), Color.yellow, 60f);
            if (cubeMesh.bounds.Contains(skin.transform.TransformPoint(vertex)))
            {
                Skin_indexes.Add(i);
            }
            i++;
        }
    }

    private GameObject GetSkin()
    {
        GameObject containerManager = transform.parent.gameObject;
        return containerManager.GetComponent<ContainerManager>().Skin;
    }
    
    public void SetBrushes()
    {
        MeshRenderer cubeMesh = gameObject.GetComponent<MeshRenderer>();
        Vector3[] batchVertices;
        int y = 0;
        foreach (var batch in App.ActiveCanvas.BatchManager.AllBatches())
        {
            batchVertices = batch.Geometry.m_Vertices.ToArray();
            foreach (var vertex in batchVertices)
            {
                if (cubeMesh.bounds.Contains(batch.transform.TransformPoint(vertex)))
                {
                    Brush_indexes.Add(y);
                }
                y++;
            }
        }
    }

    public static void DrawBounds(Bounds b, float delay = 0) /// debug
    {
        // bottom
        var p1 = new Vector3(b.min.x, b.min.y, b.min.z);
        var p2 = new Vector3(b.max.x, b.min.y, b.min.z);
        var p3 = new Vector3(b.max.x, b.min.y, b.max.z);
        var p4 = new Vector3(b.min.x, b.min.y, b.max.z);

        Debug.DrawLine(p1, p2, Color.blue, delay);
        Debug.DrawLine(p2, p3, Color.red, delay);
        Debug.DrawLine(p3, p4, Color.yellow, delay);
        Debug.DrawLine(p4, p1, Color.magenta, delay);

        // top
        var p5 = new Vector3(b.min.x, b.max.y, b.min.z);
        var p6 = new Vector3(b.max.x, b.max.y, b.min.z);
        var p7 = new Vector3(b.max.x, b.max.y, b.max.z);
        var p8 = new Vector3(b.min.x, b.max.y, b.max.z);

        Debug.DrawLine(p5, p6, Color.blue, delay);
        Debug.DrawLine(p6, p7, Color.red, delay);
        Debug.DrawLine(p7, p8, Color.yellow, delay);
        Debug.DrawLine(p8, p5, Color.magenta, delay);

        // sides
        Debug.DrawLine(p1, p5, Color.white, delay);
        Debug.DrawLine(p2, p6, Color.gray, delay);
        Debug.DrawLine(p3, p7, Color.green, delay);
        Debug.DrawLine(p4, p8, Color.cyan, delay);
    }
}
