﻿using UnityEngine;

public class ContainerManager : MonoBehaviour
{
    [SerializeField]
    private GameObject _skin;
    public GameObject Skin { get => _skin; }

    private Container[] _containers;
    public Container[] Containers { get => _containers; }

    private int _indexIntersection;
    public int IndexIntersection { get => _indexIntersection;  }

    public bool ExistIntersection()
    {
        return _indexIntersection >= 0;
    }

    public void CheckIntersection(in Bounds bound)
    {
        _containers = GetComponentsInChildren<Container>();
        _indexIntersection = -1;
        float distMin = Mathf.Infinity;
        for (int i = 0; i < _containers.Length; i++)
        {
            Bounds containerBound = _containers[i].gameObject.GetComponent<MeshRenderer>().bounds;
            Vector3 v = containerBound.center - bound.center;
            //if (containerBound.Contains(bound.center) && v.sqrMagnitude < distMin)
            if (containerBound.Intersects(bound) && v.sqrMagnitude < distMin)
            {
                _indexIntersection = i;
                distMin = v.sqrMagnitude;
            }
        }
    }

    public ref Container IntersectedContainer(int i)
    {
        return ref _containers[i];
    }

    public ref Container IntersectedContainer()
    {
        return ref _containers[_indexIntersection];
    }
}
