﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateClone : MonoBehaviour
{
    public bool existe = false;
    public GameObject corps;
    //public Mesh meshcorps;
    public Material texturecorps;
    public GameObject squelette;
    void Update()
    {
        if (Input.GetKey(KeyCode.L) && existe == false)
        {
            existe = true;
            creation();
        }
    }

    void creation()
    {
        GameObject terry = GameObject.Find("terry");
        var personnage = Instantiate(corps, new Vector3(0, 90, 0), Quaternion.identity);
        personnage.transform.parent = terry.transform;
        //SkinnedMeshRenderer skin = personnage.AddComponent<SkinnedMeshRenderer>() as SkinnedMeshRenderer;
        var s = personnage.GetComponent<SkinnedMeshRenderer>();
        //personnage.GetComponent<SkinnedMeshRenderer>().sharedMesh = meshcorps;
        personnage.GetComponent<SkinnedMeshRenderer>().rootBone = squelette.transform;
        personnage.GetComponent<SkinnedMeshRenderer>().material = texturecorps;
    }

}
