﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractBrush : MonoBehaviour
{
    public GameObject m_attached;
    public GameObject m_secondbody;
    // Update is called once per frame

    void Update()
    {
        /*GameObject[] brushs = GameObject.FindGameObjectsWithTag("Brush");

        foreach (GameObject brush in brushs)
        {
            combineMeshes(brush);
            Debug.Log(brush);
        }*/
        combineMeshes(m_secondbody);
    }

    void Start()
    {
        combineMeshes(m_secondbody);
    }

    /*public void combineMeshes(GameObject obj)
    {
        SkinnedMeshRenderer targetRenderer = m_attached.GetComponent<SkinnedMeshRenderer>();
        Dictionary<string, Transform> boneMap = new Dictionary<string, Transform>();
        foreach (Transform bone in targetRenderer.bones)
        {
            boneMap[bone.name] = bone;
        }

        SkinnedMeshRenderer thisRenderer = obj.GetComponent<SkinnedMeshRenderer>();

        thisRenderer = targetRenderer;        
    }*/

    public void combineMeshes(GameObject obj)
    {
        SkinnedMeshRenderer targetRenderer = obj.GetComponent<SkinnedMeshRenderer>();
        Dictionary<string, Transform> boneMap = new Dictionary<string, Transform>();
        foreach (Transform bone in targetRenderer.bones)
        {
            boneMap[bone.name] = bone;
        }

        SkinnedMeshRenderer thisRenderer = m_attached.GetComponent<SkinnedMeshRenderer>();

        thisRenderer = targetRenderer;
    }
}
