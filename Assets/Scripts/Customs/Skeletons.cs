﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skeletons : MonoBehaviour
{
    private int number = 0;
    public List<GameObject> BrasGauche;
    //public GameObject Target;
    private GameObject[] PartieAttach;
    private GameObject[] OldPartieAttach;
    public static Mesh viewedModel;
    public static Material viewedMaterial;
    public int t = 0;

    // Update is called once per frame
    void Start()
    {
        //BrasGauche = new List<GameObject>();
        Dreamteck.Splines.SplineComputer spline = GetComponent<Dreamteck.Splines.SplineComputer>();
        spline.space = Dreamteck.Splines.SplineComputer.Space.Local;
        //Mesh mesh = GenerateBoneWeightMesh(_currentSkinnedMesh, Config.boneIndex);
        /*foreach (Transform Partie in Target.GetComponentsInChildren<Transform>())
        {
            if (Partie.name == "BrasGauche")
            {
                spline.SetPoint(number++, new Dreamteck.Splines.SplinePoint(Partie.transform.position));
                BrasGauche.Add(Partie.gameObject);
            }
        }*/

        foreach(GameObject Partie in BrasGauche)
        {
            spline.SetPoint(number++, new Dreamteck.Splines.SplinePoint(Partie.transform.position));
        }
    }
    void Update()
    {
        Dreamteck.Splines.SplineComputer spline = GetComponent<Dreamteck.Splines.SplineComputer>();
        spline.SetPoint(0, new Dreamteck.Splines.SplinePoint(BrasGauche[0].transform.position));
        spline.SetPoint(1, new Dreamteck.Splines.SplinePoint(BrasGauche[1].transform.position));
        spline.SetPoint(2, new Dreamteck.Splines.SplinePoint(BrasGauche[2].transform.position));
        Dreamteck.Splines.SplineMesh splinemesh = GetComponent<Dreamteck.Splines.SplineMesh>();
        PartieAttach = GameObject.FindGameObjectsWithTag("Brush");

        if (t < 1)
        {
            foreach (GameObject DessinAttach in PartieAttach)
            {
                viewedModel = DessinAttach.GetComponent<MeshFilter>().mesh;
                viewedMaterial = DessinAttach.GetComponent<MeshRenderer>().material;
                splinemesh.AddChannel("composant").AddMesh(viewedModel);
                //splinemesh.GetChannel(t).GetMesh(0).offset = DessinAttach.transform.position;
                //Debug.Log(splinemesh.GetChannel(t).GetMesh(0).scale);
                splinemesh.GetChannel(t).GetMesh(0).doubleSided = true;
                Debug.Log(DessinAttach.transform.lossyScale);
                Debug.Log(DessinAttach.transform.localScale);
                //Debug.Log(splinemesh.GetChannel(t).GetMesh(0).scale);
                //splinemesh.GetChannel(t++).GetMesh(0).scale = new Vector3(0.4f,0.2f,5f);
                t++;
            }
        }
    }
}
