﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkinManager : MonoBehaviour
{
    [SerializeField]
    private GameObject _brushAnimator;
    [SerializeField]
    private GameObject _skin;

    [SerializeField]
    private Material _skinMaterial;

    private void Awake()
    {
        SkinnedMeshRenderer[] skinnedMeshRenderers = _skin.GetComponentsInChildren<SkinnedMeshRenderer>();
        for (int i = 0; i < skinnedMeshRenderers.Length; i++)
        {
            Color color = skinnedMeshRenderers[i].material.color;
            Material material = new Material(_skinMaterial);
            material.color = new Color(color.r, color.g, color.b, _skinMaterial.color.a);
            skinnedMeshRenderers[i].material = material;
        }
    }
    public void Animate(bool b)
    {
        TiltBrush.AttachBrushToSkin attachBrush = _brushAnimator.GetComponent<TiltBrush.AttachBrushToSkin>();
        attachBrush.Animate(b);
    }
}
