﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class testspline : MonoBehaviour
{
    private int number = 0;
    public GameObject[] PartieAttach;
    private GameObject[] OldPartieAttach;
    public static Mesh viewedModel;
    public static Material viewedMaterial;
    public int t = 0;

    // Update is called once per frame
    void Start()
    {
        Dreamteck.Splines.SplineComputer spline = GetComponent<Dreamteck.Splines.SplineComputer>();
        spline.space = Dreamteck.Splines.SplineComputer.Space.Local;
        //Mesh mesh = GenerateBoneWeightMesh(_currentSkinnedMesh, Config.boneIndex);
    }
    void Update()
    {
        Dreamteck.Splines.SplineComputer spline = GetComponent<Dreamteck.Splines.SplineComputer>();
        PartieAttach = GameObject.FindGameObjectsWithTag("Brush");

        if (t < 1)
        {
            foreach (GameObject DessinAttach in PartieAttach)
            {
                viewedModel = DessinAttach.GetComponent<MeshFilter>().sharedMesh;
                viewedMaterial = DessinAttach.GetComponent<MeshRenderer>().material;
                Dreamteck.Splines.SplineMesh splinemesh = GetComponent<Dreamteck.Splines.SplineMesh>();
                splinemesh.AddChannel("composant").AddMesh(viewedModel);
                //splinemesh.GetChannel(0).type.place;
                t++;
            }
        }
    }
}
