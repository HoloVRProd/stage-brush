﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Valve.VR;

public class Timer : MonoBehaviour
{
    public Text timerText;
    private float startTime;
    private bool stop = true;
    private bool reset = false;
    private string heures = "00";
    private string minutes = "00";
    private string seconds ="00";
    public SteamVR_Action_Boolean Stop;

    // Start is called before the first frame update
    void Start()
    {
        startTime = Time.time;
    }

    // Update is called once per frame
    void Update()
    {

        if (Stop.GetStateDown(SteamVR_Input_Sources.RightHand))
        {
            stop = !stop;
        }

        if(reset == true)
        {
            startTime = Time.time;
            reset = false;
        }

        if (stop == false)
        {
            reset = false;
            float t = Time.time - startTime;
            seconds = (t % 60).ToString("f0");
            float tminutes = ((int)t / 60);
            minutes = (tminutes % 60).ToString("f0");
            heures = ((int)tminutes / 60).ToString();
            timerText.color = Color.white;
        }
        else if (stop == true)
        {
            timerText.color = Color.red;
            reset = true;
        }

        timerText.text = heures +":" + minutes + ":" + seconds;
    }
}
