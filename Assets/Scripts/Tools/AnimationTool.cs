﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TiltBrush
{
    public class AnimationTool : BaseTool
    {
        [SerializeField]
        private GameObject skinManager;
        
        public override void EnableTool(bool bEnable)
        {
            base.EnableTool(bEnable);

            if (skinManager)
            {
                skinManager.GetComponent<SkinManager>().Animate(bEnable);
            }
            else
            {
                Debug.LogWarning("Skin Manager not attached for animation");
            }
        }
    }
}

