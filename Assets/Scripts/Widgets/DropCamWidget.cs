﻿// Copyright 2020 The Tilt Brush Authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using TMPro;
using UnityEngine;
using PathCreation;

namespace TiltBrush
{
    [System.Serializable]
    public class FrustumBeam
    {
        public Transform m_Beam;
        public Renderer m_BeamMesh;
        [NonSerialized] public Vector3 m_BaseScale;
    }

    public class DropCamWidget : GrabWidget
    {
        public enum Mode
        {
            SlowFollow,
            Stationary,
            Wobble,
            SemiCircular,
            Circular,
            Test,
            Infinity,
        }

        public const Mode kDefaultMode = Mode.Infinity;

        [SerializeField] private TextMeshPro m_TitleText;
        [SerializeField] private GameObject m_HintText;
        [SerializeField] private FrustumBeam[] m_FrustumBeams;

        [SerializeField] private Transform m_GhostMesh;

        [Header("Wobble Mode")]
        [SerializeField] private float m_WobbleSpeed;
        [SerializeField] private float m_WobbleScale;

        [Header("Circle Mode")]
        [SerializeField] public bool m_CircleAuto = false;
        [SerializeField] public float m_CircleSpeed;
        [SerializeField] public float m_CircleRadius;
        [SerializeField] public float m_DefaultCircleRadius;
        [SerializeField] public float m_CircleZoom;
        [SerializeField] private GameObject m_GuideCircleObject;

        [Header("Semi Elipse Mode")]
        [SerializeField] public GameObject m_ElipseTarget;
        [SerializeField] public GameObject m_ElipseRadiusTarget;
        [SerializeField] public float m_ElipseSpeed;
        [SerializeField] public float m_ElipseRadius;
        [SerializeField] public float m_CameraFollowUp;
        [SerializeField] public float m_DefaultElipseRadius;
        [SerializeField] public float m_ElipseZoom;

        [Header("Slow Follow Mode")]
        [SerializeField] private float m_SlowFollowSmoothing;

        [Header("Infinity Mode")]
        [SerializeField] private GameObject _pathCam;
        [SerializeField] private Transform _startCam;
        [SerializeField] private float _strokeHeightMargin = 0.5f;
        [SerializeField] private float _strokeWidthMargin = 0.5f;
        [SerializeField] private float _speed = 1;
        [SerializeField] private Regie _regie;
        [SerializeField] private float _angularSpeed = 10;
        [SerializeField] private bool _showDebugInfo = false;
        public EndOfPathInstruction _endOfPathInstruction;
        private GameObject _strokesMinY;
        private GameObject _strokesMaxY;
        private GameObject _strokesMinX;
        private GameObject _strokesMaxX;
        private Camera _camera;
        private float _distanceTravelled;
        private bool _fixedPosition;
        private bool _animateDropCam;

        [Header("Scaler")]
        [SerializeField] private GameObject m_Scene;
        [SerializeField] public float m_ScaleFactor;

        private float m_GuideBeamShowRatio;
        private Renderer[] m_Renderers;

        private Mode m_CurrentMode;

        private Vector3 m_vWobbleBase_RS;
        private Vector3 m_vCircleBase_RS;
        private Vector3 m_vElipseBase_RS;

        private float m_AnimatedPathTime;
        private Quaternion m_CircleOrientation;
        private Quaternion m_ElipseOrientation;
        private float m_CircleRadians;
        private float m_ElipseRadians;

        private Vector3 m_SlowFollowMoveVel;
        private Vector3 m_SlowFollowRotVel;

        public bool FixedPosition { get => _fixedPosition; set => _fixedPosition = value; }
        public bool AnimateDropCam { get => _animateDropCam; set => _animateDropCam = value; }

        override protected void Awake()
        {
            base.Awake();
            m_Renderers = GetComponentsInChildren<Renderer>();

            //initialize beams
            for (int i = 0; i < m_FrustumBeams.Length; ++i)
            {
                //cache scale and set to zero to prep for first time use
                m_FrustumBeams[i].m_BaseScale = m_FrustumBeams[i].m_Beam.localScale;
                m_FrustumBeams[i].m_Beam.localScale = Vector3.zero;
            }

            m_GuideBeamShowRatio = 0.0f;
            m_CurrentMode = kDefaultMode;
            ResetCam();

            // Register the drop camera with scene settings
            _camera = GetComponentInChildren<Camera>();
            SceneSettings.m_Instance.RegisterCamera(_camera);

            InitSnapGhost(m_GhostMesh, transform);

            _fixedPosition = false;
            _animateDropCam = true;
            _strokesMinY = new GameObject("StrokeMinY");
            _strokesMinY.transform.SetParent(App.Scene.MainCanvas.transform);
            _strokesMaxY = new GameObject("StrokeMaxY");
            _strokesMaxY.transform.SetParent(App.Scene.MainCanvas.transform);

            _strokesMinX = new GameObject("StrokeMinX");
            _strokesMinX.transform.SetParent(App.Scene.MainCanvas.transform);
            _strokesMaxX = new GameObject("StrokeMaxX");
            _strokesMaxX.transform.SetParent(App.Scene.MainCanvas.transform);

            _distanceTravelled = 0;
        }

        protected override TrTransform GetDesiredTransform(TrTransform xf_GS)
        {
            if (SnapEnabled)
            {
                return GetSnappedTransform(xf_GS);
            }
            return xf_GS;
        }

        protected override TrTransform GetSnappedTransform(TrTransform xf_GS)
        {
            TrTransform outXf_GS = xf_GS;

            Vector3 forward = xf_GS.rotation * Vector3.forward;
            forward.y = 0;
            outXf_GS.rotation = Quaternion.LookRotation(forward);

            Vector3 grabSpot = InputManager.m_Instance.GetControllerPosition(m_InteractingController);
            Vector3 grabToCenter = xf_GS.translation - grabSpot;
            outXf_GS.translation = grabSpot +
                grabToCenter.magnitude * (grabToCenter.y > 0 ? Vector3.up : Vector3.down);

            return outXf_GS;
        }

        override public void Show(bool bShow, bool bPlayAudio = true)
        {
            base.Show(bShow, bPlayAudio);

            RefreshRenderers();
        }

        public double GetYCenterFromScaleFactor(float scalefactor)
        {
            //return 0.91234 * Math.Log(7.92169* scalefactor - 1.38873) + 9.34827;
            //faire un slider
            return 0.91234 * Math.Log(7.92169 * scalefactor - 1.38873) + 9.34827;
        }

        override protected void OnShow()
        {
            base.OnShow();

            TrTransform xfSpawn = TrTransform.FromTransform(ViewpointScript.Head);
            InitIntroAnim(xfSpawn, xfSpawn, true);
            m_IntroAnimState = IntroAnimState.In;
            m_IntroAnimValue = 0.0f;
        }

        override protected void UpdateIntroAnimState()
        {
            IntroAnimState prevState = m_IntroAnimState;
            base.UpdateIntroAnimState();

            // If we're exiting the in state, notify our panel.
            if (prevState != m_IntroAnimState)
            {
                if (m_IntroAnimState == IntroAnimState.On)
                {
                    ResetCam();
                }
            }
        }

        static public string GetModeName(Mode mode)
        {
            switch (mode)
            {
                case Mode.SlowFollow: return "Head Camera";
                case Mode.Stationary: return "Stationary";
                case Mode.Wobble: return "Figure 8";
                case Mode.SemiCircular: return "Semi Elipse";
                case Mode.Circular: return "Circular";
                case Mode.Infinity: return "Infinity";
                case Mode.Test: return "Test";
            }
            return "";
        }

        void ResetCamold()
        {

            // Reset wobble cam
            m_AnimatedPathTime = (float)(0.5 * Math.PI);
            m_vWobbleBase_RS = Coords.AsRoom[transform].translation;

            // Figure out which way points in for circle cam.
            Vector3 vInwards = transform.forward;
            vInwards.y = 0;
            vInwards.Normalize();

            // Set the center of the circle that we rotate around.
            m_vCircleBase_RS = transform.position - m_DefaultCircleRadius * vInwards;
            m_CircleRadians = (float)Math.Atan2(-vInwards.z, -vInwards.x);

            // Set the center of the circle that we rotate around.
            m_ElipseRadians = (float)Math.Atan2(-vInwards.z, -vInwards.x);
            m_vElipseBase_RS = m_DefaultCircleRadius * vInwards;

            // Set the initial orientation for circle cam.
            Quaternion qCamOrient = transform.rotation;
            Vector3 eulers = new Vector3(0, (float)(m_CircleRadians * Mathf.Rad2Deg), 0);
            m_CircleOrientation = Quaternion.Euler(eulers) * qCamOrient;

            // Set the initial orientation for circle cam.
            Vector3 eieulers = new Vector3(0, 0, 5);
            m_ElipseOrientation = Quaternion.Euler(eieulers);

            // Position the guide circle.
            m_GuideCircleObject.transform.localPosition = Quaternion.Inverse(transform.rotation) * Quaternion.Euler(0, (float)(-m_CircleRadians * Mathf.Rad2Deg), 0) * new Vector3(-m_DefaultCircleRadius, 0, 0);
            m_GuideCircleObject.transform.localRotation = Quaternion.Inverse(transform.rotation) * Quaternion.Euler(0, (float)(-m_CircleRadians * Mathf.Rad2Deg), 0);
            m_GuideCircleObject.transform.localScale = 2.0f * m_DefaultCircleRadius * Vector3.one;

            // On slow follow reset, snap to head.
            if (m_CurrentMode == Mode.SlowFollow)
            {
                transform.position = ViewpointScript.Head.position;
            }
        }

        void ResetCam()
        {
            // Reset wobble cam
            m_AnimatedPathTime = (float)(0.5 * Math.PI);
            m_vWobbleBase_RS = Coords.AsRoom[transform].translation;

            // Figure out which way points in for circle cam.
            Vector3 vInwards = m_ElipseTarget.transform.position - ViewpointScript.Head.position;
            vInwards.y = 0;
            vInwards.Normalize();

            //Distance
            float fDistance = Vector3.Distance(m_ElipseTarget.transform.position, m_ElipseRadiusTarget.transform.position);

            // Set the center of the circle that we rotate around.
            m_vCircleBase_RS = m_ElipseTarget.transform.position - fDistance * vInwards;
            m_CircleRadians = (float)Math.Atan2(-vInwards.z, -vInwards.x);

            // Set the center of the circle that we rotate around.
            m_ElipseRadians = (float)Math.Atan2(-vInwards.z, -vInwards.x);
            m_vElipseBase_RS = m_DefaultCircleRadius * vInwards;

            // Set the initial orientation for circle cam.
            Quaternion qCamOrient = transform.rotation;
            Vector3 eulers = new Vector3(0, (float)(m_CircleRadians * Mathf.Rad2Deg), 0);
            m_CircleOrientation = Quaternion.Euler(eulers) * qCamOrient;

            // Set the initial orientation for circle cam.
            Vector3 eieulers = new Vector3(0, 0, 5);
            m_ElipseOrientation = Quaternion.Euler(eieulers);

            // Position the guide circle.
            m_GuideCircleObject.transform.localPosition = Quaternion.Inverse(transform.rotation) * Quaternion.Euler(0, (float)(-m_CircleRadians * Mathf.Rad2Deg), 0) * new Vector3(-m_DefaultCircleRadius, 0, 0);
            m_GuideCircleObject.transform.localRotation = Quaternion.Inverse(transform.rotation) * Quaternion.Euler(0, (float)(-m_CircleRadians * Mathf.Rad2Deg), 0);
            m_GuideCircleObject.transform.localScale = 2.0f * m_DefaultCircleRadius * Vector3.one;

            // On slow follow reset, snap to head.
            if (m_CurrentMode == Mode.SlowFollow)
            {
                transform.position = ViewpointScript.Head.position;
            }
        }

        override protected void OnUpdate()
        {
            //Set center Y position
            //Vector3 centery = m_ElipseTarget.transform.localPosition;
            //centery.y = (float)GetYCenterFromScaleFactor(m_Scene.transform.localScale.magnitude);
            //m_ElipseTarget.transform.localPosition = centery;

#if (UNITY_EDITOR || EXPERIMENTAL_ENABLED)
            if (Debug.isDebugBuild && Config.IsExperimental)
            {
                if (InputManager.m_Instance.GetKeyboardShortcutDown(
                    InputManager.KeyboardShortcut.ToggleHeadStationaryOrWobble))
                {
                    m_CurrentMode = (m_CurrentMode == Mode.Wobble) ? Mode.Stationary : Mode.Wobble;
                    RefreshRenderers();
                }
                else if (InputManager.m_Instance.GetKeyboardShortcutDown(
                    InputManager.KeyboardShortcut.ToggleHeadStationaryOrFollow))
                {
                    m_CurrentMode = (m_CurrentMode == Mode.SlowFollow) ? Mode.Stationary : Mode.SlowFollow;
                    RefreshRenderers();
                }
            }
#endif

            //animate the guide beams in and out, relatively to activation
            float fShowRatio = GetShowRatio();

            //if our transform changed, update the beams
            if (m_GuideBeamShowRatio != fShowRatio)
            {
                for (int i = 0; i < m_FrustumBeams.Length; ++i)
                {
                    //update scale
                    Vector3 vScale = m_FrustumBeams[i].m_BaseScale;
                    vScale.z *= fShowRatio;
                    m_FrustumBeams[i].m_Beam.localScale = vScale;
                }
            }
            m_GuideBeamShowRatio = fShowRatio;

            if (m_GuideBeamShowRatio >= 1.0f)
            {
                switch (m_CurrentMode)
                {
                    case Mode.Wobble:
                        if (m_UserInteracting)
                        {
                            ResetCam();
                        }
                        else
                        {
                            m_AnimatedPathTime += Time.deltaTime * m_WobbleSpeed;
                            Vector3 vWidgetPos = m_vWobbleBase_RS;

                            //sideways figure 8, or infinity symbol path
                            float fCosTime = Mathf.Cos(m_AnimatedPathTime);
                            float fSinTime = Mathf.Sin(m_AnimatedPathTime);
                            float fSqrt2 = Mathf.Sqrt(2.0f);
                            float fDenom = fSinTime * fSinTime + 1.0f;

                            float fX = (m_WobbleScale * fSqrt2 * fCosTime) / fDenom;
                            float fY = (m_WobbleScale * fSqrt2 * fCosTime * fSinTime) / fDenom;

                            vWidgetPos += transform.right * fX * m_WobbleScale;
                            vWidgetPos += transform.up * fY * m_WobbleScale;
                            transform.position = vWidgetPos;
                        }
                        break;
                    case Mode.SlowFollow:
                        {
#if (UNITY_EDITOR || EXPERIMENTAL_ENABLED)
                            if (Debug.isDebugBuild && Config.IsExperimental)
                            {
                                if (InputManager.m_Instance.GetKeyboardShortcutDown(
                                    InputManager.KeyboardShortcut.DecreaseSlowFollowSmoothing))
                                {
                                    m_SlowFollowSmoothing -= 0.001f;
                                    m_SlowFollowSmoothing = Mathf.Max(m_SlowFollowSmoothing, 0.0f);
                                }
                                else if (InputManager.m_Instance.GetKeyboardShortcutDown(
                                    InputManager.KeyboardShortcut.IncreaseSlowFollowSmoothing))
                                {
                                    m_SlowFollowSmoothing += 0.001f;
                                }
                            }
#endif
                            transform.position = Vector3.SmoothDamp(transform.position, ViewpointScript.Head.position,
                                ref m_SlowFollowMoveVel, m_SlowFollowSmoothing, Mathf.Infinity, Time.deltaTime);

                            Vector3 eulers = transform.rotation.eulerAngles;
                            Vector3 targetEulers = ViewpointScript.Head.eulerAngles;

                            eulers.x = Mathf.SmoothDampAngle(eulers.x, targetEulers.x,
                                ref m_SlowFollowRotVel.x, m_SlowFollowSmoothing, Mathf.Infinity, Time.deltaTime);
                            eulers.y = Mathf.SmoothDampAngle(eulers.y, targetEulers.y,
                                ref m_SlowFollowRotVel.y, m_SlowFollowSmoothing, Mathf.Infinity, Time.deltaTime);
                            eulers.z = Mathf.SmoothDampAngle(eulers.z, targetEulers.z,
                                ref m_SlowFollowRotVel.z, m_SlowFollowSmoothing, Mathf.Infinity, Time.deltaTime);

                            transform.rotation = Quaternion.Euler(eulers);

                            //Vector3 eulers = new Vector3(0, (float)(-m_CircleRadians * Mathf.Rad2Deg), 0);
                            //transform.rotation = Quaternion.Euler(eulers) * m_CircleOrientation;


                        }
                        break;
                    case Mode.SemiCircular:
                        if (m_UserInteracting)
                        {
                            ResetCam();
                            if (!m_GuideCircleObject.activeSelf)
                            {
                                m_GuideCircleObject.GetComponentInChildren<MeshRenderer>().material
                                    .SetFloat("_RevealStartTime", Time.time);
                                m_GuideCircleObject.SetActive(true);
                            }
                        }
                        else
                        {
                            Vector3 vInwards = m_ElipseTarget.transform.position - ViewpointScript.Head.position;
                            vInwards.y = 0;
                            vInwards.Normalize();

                            m_AnimatedPathTime += Time.deltaTime * m_WobbleSpeed;

                            //sideways figure 8, or infinity symbol path
                            float fCosTime = Mathf.Cos(m_AnimatedPathTime);
                            float fSinTime = Mathf.Sin(m_AnimatedPathTime);
                            float fSqrt2 = Mathf.Sqrt(2.0f);
                            float fDenom = fSinTime * fSinTime + 1.0f;

                            float fY = (m_WobbleScale * fSqrt2 * fCosTime * 0.2f) / fDenom;
                            float fX = (m_WobbleScale * fSqrt2 * fCosTime * fSinTime) / fDenom;

                            Vector3 vWidgetPos = m_vWobbleBase_RS;

                            //Controller
                            if (Input.GetKey(KeyCode.Space) && m_CircleAuto == false)
                            {
                                m_CircleAuto = true;
                            }
                            else if (Input.GetKey(KeyCode.Space) && m_CircleAuto == true)
                            {
                                m_CircleAuto = false;
                            }

                            if (m_CircleAuto == true)
                            {
                                vWidgetPos += transform.right * fX * m_WobbleScale;
                                vWidgetPos += transform.up * fY * m_WobbleScale;
                            }

                            if (Input.GetKey(KeyCode.UpArrow) && !Input.GetKey(KeyCode.DownArrow))
                            {

                                vWidgetPos += transform.right * fX * m_WobbleScale;
                                vWidgetPos += transform.up * fY * m_WobbleScale;
                                Debug.Log("Forward");
                                m_CircleAuto = false;
                            }
                            else if (Input.GetKey(KeyCode.DownArrow) && !Input.GetKey(KeyCode.UpArrow))
                            {
                                vWidgetPos += -transform.right * fX * m_WobbleScale;
                                vWidgetPos += -transform.up * fY * m_WobbleScale;
                                Debug.Log("Backward");
                                m_CircleAuto = false;
                            }

                            // Set the center of the circle that we rotate around.
                            //var tmp = ViewpointScript.Head.position + vWidgetPos - m_DefaultCircleRadius * vInwards;

                            //Distance
                            float fDistance = Vector3.Distance(m_ElipseTarget.transform.position, m_ElipseRadiusTarget.transform.position);

                            // Set the center of the circle that we rotate around.
                            var tmp = m_ElipseTarget.transform.position - fDistance * vInwards;
                            transform.position = Vector3.SmoothDamp(transform.position, tmp,
                               ref m_SlowFollowMoveVel, m_SlowFollowSmoothing, Mathf.Infinity, Time.deltaTime);

                            // Point toward the main target
                            transform.LookAt(m_ElipseTarget.transform, Vector3.up);

                            /*
                            //Zoom with FOV
                            Camera mycam = GetComponentInChildren<Camera>(true);
                            mycam.fieldOfView = m_ScaleFactor * m_Scene.transform.localScale.magnitude;

                            
                            if (m_Scene.transform.localScale.magnitude > 0 && m_Scene.transform.localScale.magnitude < 3)
                            {
                                m_ScaleFactor = 6f;
                            }
                            else if (m_Scene.transform.localScale.magnitude >= 3 && m_Scene.transform.localScale.magnitude < 6)
                            {
                                m_ScaleFactor = 7f;
                            }
                            else if (m_Scene.transform.localScale.magnitude >= 6 && m_Scene.transform.localScale.magnitude < 9)
                            {
                                m_ScaleFactor = 1f;
                            }
                            else if (m_Scene.transform.localScale.magnitude >= 9 && m_Scene.transform.localScale.magnitude < 12)
                            {
                                m_ScaleFactor = 0.5f;
                            }
                            else if (m_Scene.transform.localScale.magnitude >= 12 && m_Scene.transform.localScale.magnitude < 15)
                            {
                                m_ScaleFactor = 1f;
                            }
                            else if (m_Scene.transform.localScale.magnitude >= 15 && m_Scene.transform.localScale.magnitude < 18)
                            {
                                m_ScaleFactor = 2f;
                            }
                            */
                            Debug.Log(m_Scene.transform.localScale.magnitude);




                            // Deactivate the guide circle.
                            m_GuideCircleObject.SetActive(false);
                        }
                        break;
                    case Mode.Circular:
                        if (m_UserInteracting)
                        {
                            ResetCam();
                            if (!m_GuideCircleObject.activeSelf)
                            {
                                m_GuideCircleObject.GetComponentInChildren<MeshRenderer>().material
                                    .SetFloat("_RevealStartTime", Time.time);
                                m_GuideCircleObject.SetActive(true);
                            }
                        }
                        else
                        {
                            if (Input.GetKey(KeyCode.DownArrow) && !Input.GetKey(KeyCode.LeftArrow))
                            {
                                m_CircleRadius = m_CircleRadius + m_CircleZoom;
                                Debug.Log("Zoom");
                            }
                            else if (Input.GetKey(KeyCode.UpArrow) && m_CircleRadius >= 0 && !Input.GetKey(KeyCode.LeftArrow) && !Input.GetKey(KeyCode.RightArrow))
                            {
                                m_CircleRadius = m_CircleRadius - m_CircleZoom;
                                Debug.Log("Dezoom");
                            }

                            if (Input.GetKey(KeyCode.Space) && m_CircleAuto == false)
                            {
                                m_CircleAuto = true;
                            }
                            else if (Input.GetKey(KeyCode.Space) && m_CircleAuto == true)
                            {
                                m_CircleAuto = false;
                            }

                            if (m_CircleAuto == true)
                            {
                                m_CircleRadians -= (float)(Time.deltaTime * m_CircleSpeed * 2 * Math.PI);
                            }

                            if (Input.GetKey(KeyCode.LeftArrow) && !Input.GetKey(KeyCode.UpArrow) && !Input.GetKey(KeyCode.DownArrow))
                            {
                                m_CircleRadians -= (float)(Time.deltaTime * m_CircleSpeed * 2 * Math.PI);
                                Debug.Log("Left");
                                m_CircleAuto = false;
                            }
                            else if (Input.GetKey(KeyCode.RightArrow) && !Input.GetKey(KeyCode.UpArrow) && !Input.GetKey(KeyCode.DownArrow))
                            {
                                m_CircleRadians += (float)(Time.deltaTime * m_CircleSpeed * 2 * Math.PI);
                                Debug.Log("Right");
                                m_CircleAuto = false;
                            }

                            // Set the camera position.
                            Vector3 vWidgetPos = m_vCircleBase_RS;
                            vWidgetPos[0] += m_CircleRadius * (float)Math.Cos(m_CircleRadians);
                            vWidgetPos[2] += m_CircleRadius * (float)Math.Sin(m_CircleRadians);
                            transform.position = vWidgetPos;

                            // Set the camera orientation.
                            Vector3 eulers = new Vector3(0, (float)(-m_CircleRadians * Mathf.Rad2Deg), 0);
                            transform.rotation = Quaternion.Euler(eulers) * m_CircleOrientation;

                            // Deactivate the guide circle.
                            m_GuideCircleObject.SetActive(false);
                        }
                        break;

                    case Mode.Infinity:
                        if (m_UserInteracting)
                        {
                            ResetCam();
                            if (!m_GuideCircleObject.activeSelf)
                            {
                                m_GuideCircleObject.GetComponentInChildren<MeshRenderer>().material
                                    .SetFloat("_RevealStartTime", Time.time);
                                m_GuideCircleObject.SetActive(true);
                            }
                        }
                        else
                        {
                            //Controller
                            if (_pathCam)
                            {
                                ComputeStrokesMinMax();

                                Vector3 localTarget = ComputeLocalTarget();
                                Vector3 position = ComputePosition(localTarget);
                                if (PointerManager.m_Instance.IsMainPointerCreatingStroke())
                                {
                                    _pathCam.transform.position = Vector3.SmoothDamp(_pathCam.transform.position, position,
                                       ref m_SlowFollowMoveVel, m_SlowFollowSmoothing, Mathf.Infinity, Time.deltaTime);
                                }
                                else
                                {
                                    _pathCam.transform.position = position;
                                }

                                Vector3 target = App.Scene.MainCanvas.transform.TransformPoint(localTarget);
                                _pathCam.transform.LookAt(target, Vector3.up);
                                _pathCam.GetComponent<PathScale>().ChangeScale();

                                PathCreator pathCreator = _pathCam.GetComponentInChildren<PathCreator>();
                                if (_animateDropCam)
                                {
                                    /**/
                                    float r = Vector3.Distance(position, target);
                                    float speed = (_speed * r ) / 10;
                                    speed = (speed > _speed) ? _speed : speed;
                                    _distanceTravelled += speed * Time.deltaTime; 
                                    transform.position = pathCreator.path.GetPointAtDistance(_distanceTravelled, _endOfPathInstruction);
                                    /*/
                                    transform.position = _pathCam.transform.position;
                                    /**/
                                }
                                else 
                                {
                                    transform.position = pathCreator.path.GetPointAtDistance(_distanceTravelled, _endOfPathInstruction);
                                }
                                transform.LookAt(target, Vector3.up);

                                ShowSphereDBG("DBG-MINY", _strokesMinY.transform.position, Color.white);
                                ShowSphereDBG("DBG-MAXY", _strokesMaxY.transform.position, Color.green);
                                ShowSphereDBG("DBG-MINX", _strokesMinX.transform.position, Color.red);
                                ShowSphereDBG("DBG-MAXX", _strokesMaxX.transform.position, Color.blue);
                                ShowSphereDBG("DBG-TARGET", target, Color.yellow);

                                ShowFOVAxis();
                            }
                            else
                            {
                                //vInwards
                                Vector3 vInwards = m_ElipseTarget.transform.position - ViewpointScript.Head.position;
                                if (_fixedPosition)
                                {
                                    vInwards = m_ElipseRadiusTarget.transform.position - m_ElipseTarget.transform.position;
                                }
                                vInwards.y = 0;
                                vInwards.Normalize();

                                // Set the center of the circle that we rotate around.
                                float fDistance = Vector3.Distance(m_ElipseTarget.transform.position, m_ElipseRadiusTarget.transform.position);
                                var tmp = m_ElipseTarget.transform.position - fDistance * vInwards;

                                transform.position = Vector3.SmoothDamp(transform.position, tmp,
                                   ref m_SlowFollowMoveVel, m_SlowFollowSmoothing, Mathf.Infinity, Time.deltaTime);
                                // Point toward the main target
                                transform.LookAt(m_ElipseTarget.transform, Vector3.up);
                            }

                            // Deactivate the guide circle.
                            m_GuideCircleObject.SetActive(false);
                        }
                        break;
                    case Mode.Test:
                        if (m_UserInteracting)
                        {
                            ResetCam();
                            if (!m_GuideCircleObject.activeSelf)
                            {
                                m_GuideCircleObject.GetComponentInChildren<MeshRenderer>().material
                                    .SetFloat("_RevealStartTime", Time.time);
                                m_GuideCircleObject.SetActive(true);
                            }

                        }
                        else
                        {
                            Vector3 vInwards = ViewpointScript.Head.forward;
                            vInwards.y = 0;
                            vInwards.Normalize();

                            // Set the center of the circle that we rotate around.
                            var tmp = ViewpointScript.Head.position - m_DefaultCircleRadius * vInwards;
                            transform.position = Vector3.SmoothDamp(transform.position, tmp,
                               ref m_SlowFollowMoveVel, m_SlowFollowSmoothing, Mathf.Infinity, Time.deltaTime);

                            Vector3 eulers = transform.rotation.eulerAngles;
                            Vector3 targetEulers = ViewpointScript.Head.eulerAngles;

                            eulers.x = Mathf.SmoothDampAngle(eulers.x, targetEulers.x,
                                ref m_SlowFollowRotVel.x, m_SlowFollowSmoothing, Mathf.Infinity, Time.deltaTime);
                            eulers.y = Mathf.SmoothDampAngle(eulers.y, targetEulers.y,
                                ref m_SlowFollowRotVel.y, m_SlowFollowSmoothing, Mathf.Infinity, Time.deltaTime);
                            eulers.z = Mathf.SmoothDampAngle(eulers.z, targetEulers.z,
                                ref m_SlowFollowRotVel.z, m_SlowFollowSmoothing, Mathf.Infinity, Time.deltaTime);

                            transform.rotation = Quaternion.Euler(eulers);
                        }
                        break;
                }
            }

            Debug.DrawLine(transform.position, m_ElipseTarget.transform.position, new Color(1, 0, 0));
            Debug.DrawLine(m_ElipseTarget.transform.position, m_ElipseRadiusTarget.transform.position, new Color(0, 1, 0));
            Debug.DrawLine(ViewpointScript.Head.position, transform.position, new Color(0, 0, 1));
        }

        private void InitStrokesMinMax()
        {
            Vector3 ptMinY = _strokesMinY.transform.localPosition;
            Vector3 ptMaxY = _strokesMaxY.transform.localPosition;
            Vector3 ptMinX = _strokesMinX.transform.localPosition;
            Vector3 ptMaxX = _strokesMaxX.transform.localPosition;

            if(ptMinY != Vector3.zero || ptMaxY != Vector3.zero || ptMinX != Vector3.zero || ptMaxX != Vector3.zero)
            {
                return;
            }

            //Initialisation
            
            ptMinY = m_ElipseTarget.transform.localPosition;
            ptMinY.y = _startCam.localPosition.y - _strokeHeightMargin;

            ptMaxY = m_ElipseTarget.transform.localPosition;
            ptMaxY.y = _startCam.localPosition.y + _strokeHeightMargin;

            ptMinX = m_ElipseTarget.transform.localPosition;
            ptMinX.y = _startCam.localPosition.y;
            ptMinX.x -= _strokeWidthMargin;

            ptMaxX = m_ElipseTarget.transform.localPosition;
            ptMaxX.y = _startCam.localPosition.y;
            ptMaxX.x += _strokeWidthMargin;

            _strokesMinY.transform.position = App.Scene.MainCanvas.transform.TransformPoint(ptMinY);
            _strokesMaxY.transform.position = App.Scene.MainCanvas.transform.TransformPoint(ptMaxY);
            _strokesMinX.transform.position = App.Scene.MainCanvas.transform.TransformPoint(ptMinX);
            _strokesMaxX.transform.position = App.Scene.MainCanvas.transform.TransformPoint(ptMaxX);

            if(ExistAnimationStrokes()) 
            {
                //file loaded or existing stroke before we apply cam spectator
                InitMinMaxOfAnimationStrokes();
            }
        }

        private void InitMinMaxOfAnimationStrokes()
        {
            foreach (var batch in App.Scene.MainCanvas.BatchManager.AllBatches())
            {
                if (IsParticle(batch))
                {
                    continue;
                }

                Vector3[] batchVertices;
                if (batch.Geometry.m_Vertices != null)
                {
                    batchVertices = batch.Geometry.m_Vertices.ToArray();
                }
                else
                {
                    batchVertices = batch.Geometry.GetBackingMesh().vertices;
                }

                foreach (Vector3 vertex in batchVertices)
                {
                    SetMinMax(vertex);
                }
            }
        }

        private bool IsParticle(Batch batch)
        {
            BrushDescriptor desc = BrushCatalog.m_Instance.GetBrush(batch.ParentPool.m_BrushGuid);
            return desc.m_BrushPrefab.GetComponent<GeniusParticlesBrush>() != null;
        }

        /// <summary>
        /// Compute the camera target in Maincanvas coordinate system
        /// It is the middle of min height and the max height of the strokes + margins
        /// </summary>
        /// <returns></returns>
        private Vector3 ComputeLocalTarget()
        {
            //if (!ExistAnimationStrokes())
            //{
            //    Vector3 localTarget = _startCam.transform.localPosition;
            //    localTarget.z = m_ElipseTarget.transform.localPosition.z;
            //    return localTarget;
            //}
            //calculer la target locale
            return (_strokesMaxY.transform.localPosition + _strokesMinY.transform.localPosition) / 2;
        }

        private void ComputeStrokesMinMax()
        {
            if (!PointerManager.m_Instance.IsMainPointerCreatingStroke())
            {
                return;
            }

            //Get the pointer position
            Vector3 pointerPos = PointerManager.m_Instance.MainPointer.transform.position;
            pointerPos = App.Scene.MainCanvas.transform.InverseTransformPoint(pointerPos);
            pointerPos.z = m_ElipseTarget.transform.localPosition.z; // on travaille sur le même z que les ptMin
            SetMinMax(pointerPos);
            if(PointerManager.m_Instance.SymmetryModeEnabled)
            {
                Plane p = PointerManager.m_Instance.SymmetryPlane_RS.Value;
                if( p.GetDistanceToPoint(_strokesMaxX.transform.position) > p.GetDistanceToPoint(_strokesMinX.transform.position))
                {
                    _strokesMinX.transform.position = p.ReflectPoint(_strokesMaxX.transform.position);
                }
                else
                {
                    _strokesMaxX.transform.position = p.ReflectPoint(_strokesMinX.transform.position);
                }
            }
        }

        private void SetMinMax(Vector3 point)
        {
            Vector3 ptMinY = _strokesMinY.transform.localPosition;
            Vector3 ptMaxY = _strokesMaxY.transform.localPosition;

            //compute MIN MAX along height
            if (point.y - _strokeHeightMargin < ptMinY.y)
            {
                Vector3 tmp = point - new Vector3(0, _strokeHeightMargin, 0); ;
                ptMinY = tmp;
                _strokesMinY.transform.position = App.Scene.MainCanvas.transform.TransformPoint(ptMinY);
            }

            if (point.y + _strokeHeightMargin > ptMaxY.y)
            {
                Vector3 tmp = point + new Vector3(0, _strokeHeightMargin, 0);
                ptMaxY = tmp;
                _strokesMaxY.transform.position = App.Scene.MainCanvas.transform.TransformPoint(ptMaxY);
            }
        
            Vector3 ptMinX = _strokesMinX.transform.localPosition;
            Vector3 ptMaxX = _strokesMaxX.transform.localPosition;
            //compute MIN MAX along width
            if (point.x - _strokeWidthMargin < ptMinX.x)
            {
                Vector3 tmp = point - new Vector3(_strokeWidthMargin, 0, 0); ;
                ptMinX = tmp;
                _strokesMinX.transform.position = App.Scene.MainCanvas.transform.TransformPoint(ptMinX);
            }

            if (point.x + _strokeWidthMargin > ptMaxX.x)
            {
                Vector3 tmp = point + new Vector3(_strokeWidthMargin, 0, 0);
                ptMaxX = tmp;
                _strokesMaxX.transform.position = App.Scene.MainCanvas.transform.TransformPoint(ptMaxX);
            }
        }
        
        private void ShowSphereDBG(string name, Vector3 position, Color color)
        {
            if (_showDebugInfo)
            {
                GameObject go = GameObject.Find(name);
                if (go == null)
                {
                    go = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                    go.name = name;
                    go.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
                    go.transform.SetParent(App.Scene.transform);
                }
                else
                {
                    go.SetActive(true);
                }

                go.GetComponent<MeshRenderer>().material.color = color;
                go.transform.position = position;
            }
            else
            {
                GameObject go = GameObject.Find(name);
                if (go)
                {
                    go.SetActive(false);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="localTarget"> camera focus </param>
        /// <returns>return the position of dropcam widget</returns>
        private Vector3 ComputePosition(Vector3 localTarget)
        {
            if (!ExistAnimationStrokes() && !PointerManager.m_Instance.IsMainPointerCreatingStroke())
            {
                return _startCam.transform.position;
            }

            //calculer la distance camera - target: selon l'axe Y
            Vector3 tmp = _strokesMaxY.transform.localPosition;
            tmp.x = localTarget.x;
            float fov2 = _camera.fieldOfView * 0.5f;
            float distY = Vector3.Distance(localTarget, tmp) / Mathf.Tan(fov2 * Mathf.Deg2Rad);

            //calculer la distance camera - target: selon l'axe X
            Vector3 tmpMinX = _strokesMinX.transform.localPosition;
            tmpMinX.y = localTarget.y;
            Vector3 tmpMaxX = _strokesMaxX.transform.localPosition;
            tmpMaxX.y = localTarget.y;
            Vector3 tmpX = (Vector3.Distance(tmpMinX, localTarget) > Vector3.Distance(tmpMaxX, localTarget)) ? tmpMinX : tmpMaxX;
            float distX = Vector3.Distance(localTarget, tmpX) / Mathf.Tan(fov2 * 2 * Mathf.Deg2Rad);

            //calcul de la position du dropcam widget par rapport au point target qui est le focus de la camera
            float distance = Mathf.Max(distY, distX) * App.Scene.transform.localScale.z; //on scale par rapport au scale de l'environnement

            //compute vInward
            Vector3 target = App.Scene.MainCanvas.transform.TransformPoint(localTarget);
            Vector3 origin = InwardOrigin(target);

            Vector3 vInwards = target - origin;
            vInwards.y = 0;
            vInwards.Normalize();

            Vector3 position = target - distance * vInwards;

            return position;
        }

        private Vector3 InwardOrigin(Vector3 target)
        {
            Vector3 origin = ViewpointScript.Head.position;
            if (_regie.AnimationEnabled() || _fixedPosition)
            {
                //turn from headset view to front view
                Vector3 pathCamPos = _pathCam.transform.position;
                Vector3 v = target - pathCamPos;
                v.Normalize();
                float angle = Vector3.Angle(v, m_ElipseTarget.transform.forward);
                if (Mathf.Abs(angle) > 5f)
                {
                    origin = pathCamPos;
                    origin = Vector3.RotateTowards(origin, -m_ElipseTarget.transform.forward, _angularSpeed * Time.deltaTime * Mathf.Deg2Rad, 0);
                }
                else
                {
                    origin = target - m_ElipseTarget.transform.forward;
                }
            }
            else if (ExistAnimationStrokes() && !_fixedPosition)
            {
                //turn from front to headset view
                Vector3 pathCamPos = _pathCam.transform.position;
                Vector3 v = pathCamPos - target;
                v.Normalize();

                Vector3 vOrigin = origin;
                vOrigin.y = target.y;
                Vector3 vHeadset = vOrigin - target;
                vHeadset.Normalize();
                float angle = Vector3.Angle(v, vHeadset);
                if (Mathf.Abs(angle) > 5f)
                {
                    origin = pathCamPos;
                    origin = Vector3.RotateTowards(origin, vHeadset, _angularSpeed * Time.deltaTime * Mathf.Deg2Rad, 0);
                }
            }
            return origin;
        }

        private bool ExistAnimationStrokes()
        {
            int nbBatch = 0;
            foreach (var batch in App.Scene.MainCanvas.BatchManager.AllBatches())
            {
                if (IsParticle(batch))
                {
                    continue;
                }
                else
                {
                    nbBatch++;
                }
                
            }
            return nbBatch > 0;
        }

        public Vector3 GetTarget() 
        {
            if (_pathCam)
            {
                Vector3 localTarget = ComputeLocalTarget();
                return App.Scene.MainCanvas.transform.TransformPoint(localTarget);
            }

            return Vector3.zero;
        }

        override public void Activate(bool bActive)
        {
            base.Activate(bActive);
            Color activeColor = bActive ? Color.white : Color.grey;
            m_TitleText.color = activeColor;
            m_HintText.SetActive(bActive);

            for (int i = 0; i < m_FrustumBeams.Length; ++i)
            {
                m_FrustumBeams[i].m_BeamMesh.material.color = activeColor;
            }
        }

        public void SetMode(Mode newMode)
        {
            m_CurrentMode = newMode;
            RefreshRenderers();
            ResetCam();
        }

        public Mode GetMode()
        {
            return m_CurrentMode;
        }

        void RefreshRenderers()
        {
            // Show the widget beams if we're not in slow follow mode, and we're active.
            bool bShow = ShouldHmdBeVisible();
            for (int i = 0; i < m_Renderers.Length; ++i)
            {
                m_Renderers[i].enabled = bShow;
            }
        }

        public bool ShouldHmdBeVisible()
        {
            return (m_CurrentMode != Mode.SlowFollow) &&
                (m_CurrentState == State.Showing || m_CurrentState == State.Visible);
        }

        private void OnEnable()
        {
            InitStrokesMinMax();
        }

        private void OnDisable()
        {
            if (_strokesMinY)
            {
                _strokesMinY.transform.localPosition = Vector3.zero;
            }
            if (_strokesMaxY)
            {
                _strokesMaxY.transform.localPosition = Vector3.zero;
            }
            if (_strokesMinX)
            {
                _strokesMinX.transform.localPosition = Vector3.zero;
            }
            if (_strokesMaxX)
            {
                _strokesMaxX.transform.localPosition = Vector3.zero;
            }
            _camera.transform.localPosition = Vector3.zero;
        }

        private void ShowFOVAxis()
        {
            if (_showDebugInfo)
            {
                Vector3 v = _camera.transform.forward;
                v.y = 0;
                v = Quaternion.AngleAxis(-_camera.fieldOfView / 2, _camera.transform.right) * v;
                Debug.DrawRay(_camera.transform.position, v * 20, Color.yellow);
                Vector3 v1 = _camera.transform.forward;
                v1.y = 0;
                v1 = Quaternion.AngleAxis(_camera.fieldOfView / 2, _camera.transform.right) * v1;
                Debug.DrawRay(_camera.transform.position, v1 * 20, Color.cyan);
                //Debug.Log("ANGLE" + Vector3.Angle(v1, v));
            }
        }
    }
} // namespace TiltBrush
